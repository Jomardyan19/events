﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Group group = new Group { name = "C#"};
            //group.teacher =  new Teacher
            //{
            //    name = "A1",
            //    university = new University { name = "U1" }
            //};

            if(group != null)
            {
                if(group.teacher != null)
                {
                    if(group.teacher.university != null)
                    {
                        Console.WriteLine(group.teacher.university.name);
                    }
                }
            }


            Console.WriteLine(group?.teacher?.university?.name);
        }
    }

    class Group
    {
        public string name;
        public Teacher teacher;
    }

    class Teacher
    {
        public string name;
        public University university;
    }

    class University
    {
        public string name;

    }
}
