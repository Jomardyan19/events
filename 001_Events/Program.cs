﻿using System;

namespace _001_Events
{
    public delegate void EventDelegate();

    public class MyClass
    {
        public event EventDelegate myEvent;

        public void Calculate()
        {
            //....
            myEvent.Invoke();
            //....
        }
    }

    class Program
    {
        static void Main()
        {
            var instance = new MyClass();

            instance.myEvent += new EventDelegate(Apres);
            instance.myEvent += ShatApres;
            instance.myEvent += () => Console.WriteLine("Hello");

            //instance.myEvent.Invoke();
            instance.Calculate();

            Console.WriteLine(new string('-', 20));

            instance.myEvent -= ShatApres;

            instance.Calculate();

            Console.ReadKey();
        }

        private static void Apres()
        {
            //Обработчик события
            Console.WriteLine("Apres");
        }

        private static void ShatApres()
        {
            //Обработчик события
            Console.WriteLine("Shat Apres");
        }
    }
}
