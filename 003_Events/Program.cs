﻿using System;

// Abstract and Virtal events

namespace _003_Events
{
    public delegate void EventDelegate();

    interface IInterface
    {
        event EventDelegate MyEvent; // Abstract event.
    }

    public class BaseClass : IInterface
    {
        EventDelegate myEvent;

        public virtual event EventDelegate MyEvent // Virtual event.
        {
            add { myEvent += value; }
            remove { myEvent -= value; }
        }

        public void InvokeEvent()
        {
            myEvent.Invoke();
        }
    }

    public class DerivedClass : BaseClass
    {
        public override event EventDelegate MyEvent
        {
            add
            {
                base.MyEvent += value;
                Console.WriteLine("К событию базового класса был прикреплен обработчик - {0}", value.Method.Name);
            }
            remove
            {
                base.MyEvent -= value;
                Console.WriteLine("От события базового класса был откреплен обработчик - {0}", value.Method.Name);
            }
        }
    }

    class Program
    {
        static void Main()
        {
            DerivedClass instance = new DerivedClass();

            instance.MyEvent += new EventDelegate(Handler1);
            instance.MyEvent += new EventDelegate(Handler2);

            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));

            instance.MyEvent -= new EventDelegate(Handler2);
            instance.InvokeEvent();

            Console.ReadKey();
        }

        static private void Handler1()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 1");
        }

        static private void Handler2()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 2");
        }
    }
}