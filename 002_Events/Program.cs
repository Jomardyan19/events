﻿using System;

namespace _002_Events
{
    public delegate void EventDelegate();

    public class MyClass
    {
        EventDelegate myEvent;

        // Реализация методов доступа add и remove для события.
        public event EventDelegate MyEvent
        {
            add { myEvent += value; }
            remove { myEvent -= value; }
        }

        //public void add_MyEvent(EventDelegate value)
        //{
        //    myEvent += value;
        //}

        //public void remove_MyEvent(EventDelegate value)
        //{
        //    myEvent += value;
        //}

        public void InvokeEvent()
        {
            myEvent.Invoke();
        }
    }

    class Program
    {
        static void Main()
        {
            var instance = new MyClass();

            instance.MyEvent += new EventDelegate(Handler1);
            instance.MyEvent -= Handler2;

            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));

            instance.MyEvent -= new EventDelegate(Handler2);
            instance.InvokeEvent();

            Console.ReadKey();
        }

        static private void Handler1()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 1");
        }

        static private void Handler2()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 2");
        }
    }
}
