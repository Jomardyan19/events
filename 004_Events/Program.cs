﻿using System;

namespace _004_Events
{
    public delegate void EventDelegate();

    public class MyClass
    {
        public event EventDelegate MyEvent = null;

        public void InvokeEvent()
        {
            MyEvent.Invoke();
        }
    }

    class Program
    {
        static void Main()
        {
            var instance = new MyClass();

            instance.MyEvent += new EventDelegate(Handler1);
            instance.MyEvent += new EventDelegate(Handler2);
            instance.MyEvent += delegate { Console.WriteLine("Anonymous method 1."); };

            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));

            instance.MyEvent -= new EventDelegate(Handler2);

            instance.MyEvent -= delegate { Console.WriteLine("Anonymous method 1."); };

            instance.InvokeEvent();

            // Delay.
            Console.ReadKey();
        }

        static private void Handler1()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 1");
        }

        static private void Handler2()
        {
            //Обработчик события
            Console.WriteLine("Event Handler 2");
        }
    }
}