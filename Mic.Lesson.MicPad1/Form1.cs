﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mic.Lesson.MicPad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            var dr = openFileDialog.ShowDialog();
            if(dr == DialogResult.OK)
            {
                string fileName = openFileDialog.FileName;
                richTextBox.Text = File.ReadAllText(fileName);
            }
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void saveAsToolStripButton_Click(object sender, EventArgs e)
        {
            var dr = saveFileDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;
                File.WriteAllText(fileName, richTextBox.Text);
            }
        }
    }
}
