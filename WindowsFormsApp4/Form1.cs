﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        //void EventHandler(object sender, EventArgs e);
        private void button1_Click(object sender, EventArgs e)
        {
            return;
            //DialogResult dr = MessageBox.Show("Du lav txa es?", "Test", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            while(true)
            {
                var dr = MessageBox.Show("Du lav txa es?", "Test", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.No)
                    return;
            }
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            //richTextBox1.Text += "Mouse Enter\n";
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            richTextBox1.Text += "Mouse Click\n";
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            richTextBox1.Text += "Mouse Down\n";
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            richTextBox1.Text += "Mouse Up\n";
        }
    }
}
