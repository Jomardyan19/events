﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //DialogResult dr = MessageBox.Show("hamadzajn es..", "Info", 
            //    MessageBoxButtons.YesNo, 
            //    MessageBoxIcon.Question, 
            //    MessageBoxDefaultButton.Button2);

            InsertRandomItems(listBox1, 10);
        }

        private void InsertRandomItems(ListBox listBox, int count)
        {
            listBox.Items.Clear();

            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                int item = rnd.Next(5, 20);
                listBox.Items.Add(item);
            }

            int index = rnd.Next(0, count);
            listBox.SelectedIndex = index;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object item = listBox1.SelectedItem;
            if (item == null)
                return;

            int count = (int)item;
            InsertRandomItems(listBox2, count);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            object item = listBox2.SelectedItem;
            if (item == null)
                return;

            int count = (int)item;
            InsertRandomItems(listBox3, count);
        }
    }
}
