﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    class NumericTextBox : TextBox
    {
        public double Value
        {
            get
            {
                double.TryParse(Text, out double value);
                return value;
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '\b')
                base.OnKeyPress(e);
            else
                e.Handled = true;
        }
    }
}
