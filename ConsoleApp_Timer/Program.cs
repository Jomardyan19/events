﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp_Timer
{
    public enum TimerTicks
    {
        Millisecond = 1,
        Second = Millisecond * 1000,
        Minute = Second * 60,
        Hour = Minute * 60,
        Day = 24 * Hour
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Timer t = new Timer(Callback, "test", 0, 1000);
            //Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Red;
            Tick("");
            Console.ResetColor();

            //// Create a Timer object that knows to call our TimerCallback
            //// method once every 2000 milliseconds.
            //Timer t = new Timer(Callback, "Barev", 2000, 1000);

            MyTimer myTimer = new MyTimer { TimerTick = 1 * (int)TimerTicks.Second };
            //myTimer.Tick += Callback;
            myTimer.Tick += Tick;
            myTimer.Tick += Callback;
            myTimer.TickCount = 5;
            myTimer.Start();

            // Wait for the user to hit <Enter>
            Console.ReadLine();
        }

        private static void Callback(object state)
        {
            Console.WriteLine("In TimerCallback: " + state);
        }

        private static void Tick(object state)
        {
            // Display the date/time when this method got called.
            Console.WriteLine("Tick : " + DateTime.Now.ToString("mm:ss"));
        }
    }

    class MyTimer
    {
        public MyTimer()
        {
            TimerTick = (int)TimerTicks.Second;
        }

        private uint _counter;
        public int TimerTick { get; set; }
        public uint TickCount { get; set; }
        public event TimerCallback Tick;

        private Timer _timer;

        public void Start()
        {
            _counter = 0;
            _timer = new Timer(OnTick, "Started", 0, TimerTick);
        }

        public void Stop()
        {
            _counter = 0;
            _timer?.Dispose();
            _timer = null;
        }

        void OnTick(object state)
        {
            if(TickCount == _counter)
            {
                Stop();
                Console.WriteLine("Stoped.");
            }
            else
            {
                _counter++;
                Tick.Invoke(state);
            }
        }
    }
}
