﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    static class Helper
    {
        private static List<string> items = new List<string>
        {
                "Hovhannes",
                "Vahe",
                "Ani",
                "Vachagan",
                "Vard",
                "Jirayr",
                "Vagharsh",
                "Hakob",
                "Nelli",
                "Mary",
                "VaheAAA"
            };

        public static List<Student> CreateStudents(int count)
        {
            var students = new List<Student>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var st = new Student
                {
                    Name = items[rnd.Next(0, items.Count)],
                    Surname = $"A{i + 1}yan",
                    Birthday = new DateTime(
                        rnd.Next(1992, 2002),
                        rnd.Next(1, 13),
                        rnd.Next(1, 28)
                        )
                };
                students.Add(st);
            }
            return students;
        }
    }
}
