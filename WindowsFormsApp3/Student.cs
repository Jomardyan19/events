﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Fullname => $"{Surname} {Name}";

        public DateTime Birthday { get; set; }

        public int Age => DateTime.Now.Year - Birthday.Year;

        public override string ToString()
        {
            return Fullname;
        }
    }
}